const nock = require('nock');
const fs = require('fs');
const path = require('path');

const textToArray = (text = '') => {
  return text.replace(/(\r|\n)+/g, ',').replace(/\s{1,}/g, ' ').split(',')
};

module.exports = () => {
  const scope = nock('http://www.dummy-service.com')

  const allLists = fs.readdirSync(path.resolve('./src/data'), { encoding: 'utf8' })
    .map(file => {
      const result = textToArray(fs.readFileSync(`${path.resolve('./src/data')}/${file}`, { encoding: 'utf8' }));
      scope.get(`/api/lists/city/${file.split('.')[0]}`)
        .reply(200, result);

      return result;
    });

  scope.get('/api/lists/city')
    .reply(200, allLists);
  scope.get('/api/lists/city')
    .reply(200, allLists);
}