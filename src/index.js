const axios = require('axios');
const allCities = require('all-the-cities');

const isCity = text => !!allCities.filter(city => city.name.toLowerCase() === text.toLowerCase())[0];

const matchAllCharacters = (text, compare) => (!!text.toLowerCase()
  .split('')
  .reduce((reference, char) => reference.replace(char, ''), compare));

/**
 * @function getGroups
 * @param {string[]} data The data you want to group
 * @param {object} options The options the function can use to process the data differently
 */
const getGroups = (data, options = {}) => {
  const { cityObect = false } = options;

  /* To ensure that the function remains pure, I am cloning the data object */
  const list = [...data];

  const groups = cityObect ? {} : [];
  const cityObectMap = cityObect && new WeakMap();
  /* There are more readable algorythms out there but this one has the advantage of being very performant */
  while (list[0]) {
    const newGroup = list.reduceRight((group, text, index) => {
      /* Get the key name if an object is to be generated instead of nested arrays */
      if (cityObectMap && isCity(text)) {
        cityObectMap.set(group, text);
      }

      const reference = (group[0] || '').toLowerCase();
      const remaining = matchAllCharacters(text, reference)

      /* Only push to group if there is a complete match of all the characters */
      if (!remaining) {
        group.push(list.splice(index, 1)[0]);
      }
      return group;
    }, []);

    const unique = [...(new Set(newGroup))].sort();
    if (!cityObect) {
      groups.unshift(unique);
    } else {
      groups[cityObectMap.get(newGroup)] = unique;
    }

  }
  return groups;
}

/**
 * @function getFlattenedList
 * @param {string} path The URL/endpoint where the data is located
 */
const getFlattenedList = (path) => axios.get(path)
  .then(response => [].concat.apply([], response.data));

module.exports = {
  getFlattenedList,
  getGroups
};