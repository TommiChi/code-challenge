const interceptor = require('./__mocks/http-interceptor');
interceptor();
interceptor();

const CONSTANTS = require('./modules/constants');
const cityLists = require('./index');

cityLists.getFlattenedList(`${CONSTANTS.LISTS_HOST}/${CONSTANTS.ALL_LISTS}/${CONSTANTS.LIST1}`).then(list => cityLists.getGroups(list)).then(data => console.warn('/!\\ List1 /!\\', '\n\n', data, '\n\n\n'));

cityLists.getFlattenedList(`${CONSTANTS.LISTS_HOST}/${CONSTANTS.ALL_LISTS}/${CONSTANTS.LIST2}`).then(list => cityLists.getGroups(list)).then(data => console.warn('/!\\ List2 /!\\', '\n\n', data, '\n\n\n'));

cityLists.getFlattenedList(`${CONSTANTS.LISTS_HOST}/${CONSTANTS.ALL_LISTS}`).then(list => cityLists.getGroups(list)).then(data => console.warn('/!\\ Merged lists (list1 + list2) /!\\', '\n\n', data, '\n\n\n'));

cityLists.getFlattenedList(`${CONSTANTS.LISTS_HOST}/${CONSTANTS.ALL_LISTS}`).then(list => cityLists.getGroups(list, { cityObect: true })).then(data => console.warn('/!\\ Merged lists (list1 + list2) as an object instead of a nested array /!\\', '\n\n', (data)));
