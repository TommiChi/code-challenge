const interceptor = require('../src/__mocks/http-interceptor');

const { expect } = require('chai')
const { getFlattenedList, getGroups } = require('../src/index');
const CONSTANTS = require('../src/modules/constants');

describe('Get a list', () => {
  const list1 = ['Hemarn', 'Rispa', 'Spari', 'Terdamrot', 'Amsterdam', 'Arnhem', 'Erdamamst', 'Damrotter', 'Rotterdam', 'Paris'];
  const list2 = ['Erdamamst', 'Spari', 'Paris', 'Rispa', 'Rotterdam', 'Rispa', 'Damrotter', 'Arnhem', 'Terdamrot', 'Amsterdam', 'Hemarn', 'Erdamamst'];

  beforeEach(() => {
    interceptor();
  });

  it('Should return List 1', (done) => {
    getFlattenedList(`${CONSTANTS.LISTS_HOST}/${CONSTANTS.ALL_LISTS}/${CONSTANTS.LIST1}`).then((data) => {
      expect(data instanceof Array).to.be.true;
      expect(data).to.eql(list1);
      done();
    })
  });

  it('Should return List 2', (done) => {
    getFlattenedList(`${CONSTANTS.LISTS_HOST}/${CONSTANTS.ALL_LISTS}/${CONSTANTS.LIST2}`).then((data) => {
      expect(data instanceof Array).to.be.true;
      expect(data).to.eql(list2);
      done();
    })
  });

  it('Should return a flattened array that concatenates List 1 and 2', (done) => {
    getFlattenedList(`${CONSTANTS.LISTS_HOST}/${CONSTANTS.ALL_LISTS}`).then((data) => {
      expect(data instanceof Array).to.be.true;
      expect(data).to.eql([].concat(list1, list2));
      done();
    })
  });
})

describe('Group a list', () => {
  const group1 = [ [ 'Arnhem', 'Hemarn' ],
  [ 'Amsterdam', 'Erdamamst' ],
  [ 'Damrotter', 'Rotterdam', 'Terdamrot' ],
  [ 'Paris', 'Rispa', 'Spari' ] ];
  const group2 = [ [ 'Paris', 'Rispa', 'Spari' ],
  [ 'Damrotter', 'Rotterdam', 'Terdamrot' ],
  [ 'Arnhem', 'Hemarn' ],
  [ 'Amsterdam', 'Erdamamst' ] ];
  const group3 = { Amsterdam: [ 'Amsterdam', 'Erdamamst' ],
  Arnhem: [ 'Arnhem', 'Hemarn' ],
  Rotterdam: [ 'Damrotter', 'Rotterdam', 'Terdamrot' ],
  Paris: [ 'Paris', 'Rispa', 'Spari' ] };

  beforeEach(() => {
    interceptor();
  });

  it('Should group List 1 in a nested array of cities', (done) => {
    getFlattenedList(`${CONSTANTS.LISTS_HOST}/${CONSTANTS.ALL_LISTS}/${CONSTANTS.LIST1}`).then(list => getGroups(list)).then((data) => {
      expect(data.length).to.equal(4);
      expect(data[0] instanceof Array).to.be.true;
      expect(data[1] instanceof Array).to.be.true;
      expect(data[2] instanceof Array).to.be.true;
      expect(data[3] instanceof Array).to.be.true;
      expect(data).to.eql(group1);
      done();
    })
  });

  it('Should group List 2 in a nested array of cities', (done) => {
    getFlattenedList(`${CONSTANTS.LISTS_HOST}/${CONSTANTS.ALL_LISTS}/${CONSTANTS.LIST2}`).then(list => getGroups(list)).then((data) => {
      expect(data.length).to.equal(4);
      expect(data[0] instanceof Array).to.be.true;
      expect(data[1] instanceof Array).to.be.true;
      expect(data[2] instanceof Array).to.be.true;
      expect(data[3] instanceof Array).to.be.true;
      expect(data).to.eql(group2);
      done();
    })
  });

  it('Should group Lists 1 and 2 in a nested array of cities', (done) => {
    getFlattenedList(`${CONSTANTS.LISTS_HOST}/${CONSTANTS.ALL_LISTS}`).then(list => getGroups(list)).then((data) => {
      expect(data.length).to.equal(4);
      expect(data[0] instanceof Array).to.be.true;
      expect(data[1] instanceof Array).to.be.true;
      expect(data[2] instanceof Array).to.be.true;
      expect(data[3] instanceof Array).to.be.true;
      expect(data).to.eql(group2);
      done();
    })
  });

  it('Should group Lists 1 and 2 by key, where the key name is the city', (done) => {
    getFlattenedList(`${CONSTANTS.LISTS_HOST}/${CONSTANTS.ALL_LISTS}`).then(list => getGroups(list, { cityObect: true })).then((data) => {
      const dataType = Object.prototype.toString.call(data).match(/\w{1,}(?=\])/g)[0].toLowerCase();
      expect(dataType).to.equal('object');
      expect(data).to.have.all.keys('Amsterdam', 'Arnhem', 'Rotterdam', 'Paris');
      expect(data).to.eql(group3);
      done();
    })
  });
});