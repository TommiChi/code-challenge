# NodeJS test for Code

## Test Assignment
You can find a ZIP archive as an attachment. This includes 2 txt files with each containing a list of words. Each list contains words that are matching somehow. It's up to you to figure out what the connecting part is. Group these words en sort them alphabetically within the group. Sort the groups alphabetically and use the first word of each group while sorting. One last note, I don't want duplicates in each final sort result.

A sample of the required sorting:
```
A1, A2, A3
B1
C1, C2
```

I'd like to have deliverable code in javascript that runs on Nodejs v10 LTS.
Make a test per list to demonstrate your function works. With testing, use the modules Mocha and Chai. And of course we start this from the cli with: npm test.

## Viewing the assignment
The source code of the assignment is located at `src/index.js`;

To view the assignment and its tests, you need to:

- Checkout the repository
- Run `npm install`
- To execute the assignment, run `npm start`. The results will be outputted in the terminal
- To execute the unit tests, run `npm test`

## Additional Information
To execute the assignment I used the Nock (http interceptor) library to intercept all http traffic and return the required data, which saved me from having to setup real API endpoints, while allowing for the code to make real http requests.

Because I already had this setup, I opted to simply reuse it rather than also having to go through the trouble of setting up spies (with Sinon or Chai Spies)

I also opted not to use Typescript mainly because I can churn out code faster in plain old ES6. In the context of a "real" project, I would definitely consider using Typescript if the team is on board with it.